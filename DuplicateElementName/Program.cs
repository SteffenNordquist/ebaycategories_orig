﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DuplicateElementName
{
    class Program
    {
        static void Main(string[] args)
        {
            MongoCollection<BsonDocument> collection = new MongoClient("mongodb://client148:client148devnetworks@136.243.44.111/PD_Wuerth")
                .GetServer()
                .GetDatabase("PD_Wuerth")
                .GetCollection<BsonDocument>("wuerth");

            var docs = collection.Find(Query.Exists("plus.ebaycategory")).SetFlags(QueryFlags.NoCursorTimeout);

            StringBuilder writer = new StringBuilder();
            foreach (var doc in docs)
            {
                try
                {
                    var p = BsonSerializer.Deserialize<BsonDocument>(doc);
                }
                catch
                {
                    writer.AppendLine(doc["product"]["ean"].AsString);
                }
            }

            File.WriteAllText("dupElementNames.txt", writer.ToString());
        }
    }
}
