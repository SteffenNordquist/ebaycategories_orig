﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesManual
{
    public class SupplierDBs
    {
        public static List<string> SupplierDatabaseNames()
        {
            var server = new MongoClient("mongodb://steffennordquist:adminsteffen148devnetworks@136.243.44.111/admin")
                .GetServer();

            return server.GetDatabaseNames().Select(x => x.ToString()).Where(x => x.StartsWith("PD")).ToList();
        }

        
    }
}
