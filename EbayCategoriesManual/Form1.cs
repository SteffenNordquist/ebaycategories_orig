﻿using DN.DataAccess.ConnectionFactory;
using EbayCategoriesManual.CategoryPersistent;
using EbayCategoriesManual.Document;
using EbayCategoriesManual.Document.ElementParser;
using EbayCategoriesManual.Document.FieldsSplitter;
using EbayCategoriesManual.Ebay.Searcher;
using EbayCategoriesManual.Entities;
using EbayCategoriesManual.ImageWorker;
using EbayCategoriesManual.SearchReadyItems;
using EbayCategoriesManual.Settings;
using EbayCategoriesManual.SupplierDBConstructor;
using EbayCategoriesManual.Suppliers;
using Microsoft.Practices.Unity;
using MongoDB.Bson;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
//using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace EbayCategoriesManual
{
    public partial class Form1 : Form
    {
        private string addOnSearchText = "";

        private readonly AppConfiguration appConfiguration;

        private IDbConnection selectedSupplierDB;

        private BsonDocument supplierSampleDocument;

        public Form1()
        {
            appConfiguration = new AppConfiguration(new UnityContainer());

            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            progressBar.Maximum = 100;
            progressBar.Value = 0;
            progressBar.Visible = false;
            //List<string> dbNames = SupplierDBs.SupplierDatabaseNames();
            //dbNames.ForEach(x => cmbSelectedDBName.Items.Add(x));
            PopulateSupplierDBNames();


            StartupDisableElements();
        }

        private void PopulateSupplierDBNames()
        {
            ISuppliers bkSuppliers = appConfiguration.Resolve<ISuppliers>();

            List<string> dbNames = (List<string>)bkSuppliers.GetSuppliers();

            dbNames.ForEach(x => cmbSelectedDBName.Items.Add(x));
        }

        private void StartupDisableElements()
        {
            btnSearch.Enabled = false;
            cmbSelectedSearchField.Enabled = false;
            cmbSelectedId.Enabled = false;
            btnSearchNext.Enabled = false;
            treeViewSampleDocument.Enabled = false;
            //txtCategoryId.Enabled = false;
            //btnUpdate.Enabled = false;
        }

        private void btnLoadFields_Click(object sender, EventArgs e)
        {
            if (cmbSelectedDBName.SelectedIndex > -1)
            {
                var threadLoadSelectedSupplierDBData = new Thread(() => LoadSelectedSupplierDBData());
                threadLoadSelectedSupplierDBData.Start();

                //LoadSelectedSupplierDBData();
            }
            else {
                MessageBox.Show("Select DB Supplier!");
            }

            
        }

        private void LoadSelectedSupplierDBData()
        {
            this.Invoke(new MethodInvoker(() => progressBar.Visible = true));
            this.Invoke(new MethodInvoker(() => progressBar.Maximum = 100));
            this.Invoke(new MethodInvoker(() => progressBar.Value = 10));

            IDBConstructor supplierDBConstructor = appConfiguration.Resolve<IDBConstructor>();

            this.Invoke(new MethodInvoker(() => progressBar.Value = 20));
            //constructing supplier db
            selectedSupplierDB = supplierDBConstructor.Create<string>(GetCmbSupplierDBSelectedName());

            this.Invoke(new MethodInvoker(() => progressBar.Value = 30));

            ISettings settings = new DefaultSettings();
            settings.SetValue("dbname", GetCmbSupplierDBSelectedName());
            appConfiguration.RegisterSettings(settings);

            supplierSampleDocument = selectedSupplierDB.DataAccess.Queries.Find<BsonDocument>("{ _id : {$exists:true}}", null, 0, 1).FirstOrDefault();

            this.Invoke(new MethodInvoker(() => progressBar.Value = 50));

            IDocumentFieldsSplitter documentFieldsSplitter = appConfiguration.Resolve<IDocumentFieldsSplitter>();
            List<string> fields = documentFieldsSplitter.GetFields<BsonDocument>(supplierSampleDocument);

            this.Invoke(new MethodInvoker(() => progressBar.Value = 60));
            //List<string> fields = supplierCollection.GetFields(GetCmbSupplierDBSelectedName());
            
            ClearCmbSelectedId();
            fields.ForEach(x => AddItemToCmbSelectedId(x));

            ClearCmbSelectedSearchField();
            fields.ForEach(x => AddItemToCmbSelectedSearchField(x));

            //MessageBox.Show("Fields loaded!");
            this.Invoke(new MethodInvoker(() => progressBar.Value = 70));

            EnableBtnSearch();

            EnableCmbSelectedSearchField();

            EnableCmbSelectedId();

            SampleDocumentTreeViewLoad();

            this.Invoke(new MethodInvoker(() => progressBar.Value = 100));

            Thread.Sleep(1500);

            this.Invoke(new MethodInvoker(() => progressBar.Visible = false));

            this.Invoke(new MethodInvoker(() => progressBar.Value = 0));

            
        }

        #region LoadSelectedSupplierDBData() safe threads

        private string GetCmbSupplierDBSelectedName()
        {
            string selected = "";
            if (cmbSelectedDBName.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate() { selected = cmbSelectedDBName.SelectedItem.ToString(); });
            }
            else
            {
                selected = cmbSelectedDBName.SelectedItem.ToString();
            }

            return selected;
        }

        private void ClearCmbSelectedId()
        {
            if (cmbSelectedId.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate() { cmbSelectedId.Items.Clear(); });
            }
            else
            {
                cmbSelectedId.Items.Clear();
            }

        }

        private void ClearCmbSelectedSearchField()
        {
            if (cmbSelectedSearchField.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate() { cmbSelectedSearchField.Items.Clear(); });
            }
            else
            {
                cmbSelectedSearchField.Items.Clear();
            }

        }

        private void AddItemToCmbSelectedId(string item)
        {
            if (cmbSelectedId.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate() { cmbSelectedId.Items.Add(item); });
            }
            else
            {
                cmbSelectedId.Items.Add(item);
            }
        }

        private void AddItemToCmbSelectedSearchField(string item)
        {
            if (cmbSelectedSearchField.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate() { cmbSelectedSearchField.Items.Add(item); });
            }
            else
            {
                cmbSelectedSearchField.Items.Add(item);
            }
        }

        private void EnableBtnSearch()
        {
            if (btnSearch.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate() { btnSearch.Enabled = true; });
            }
            else
            {
                btnSearch.Enabled = true;
            }
        }

        private void EnableCmbSelectedSearchField()
        {
            if (cmbSelectedSearchField.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate() { cmbSelectedSearchField.Enabled = true; cmbSelectedSearchField.SelectedIndex = 0; });
            }
            else
            {
                cmbSelectedSearchField.Enabled = true; cmbSelectedSearchField.SelectedIndex = 0;
            }
        }

        private void EnableCmbSelectedId()
        {
            if (cmbSelectedId.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate() { cmbSelectedId.Enabled = true; cmbSelectedId.SelectedIndex = 0; });
            }
            else
            {
                cmbSelectedId.Enabled = true; cmbSelectedId.SelectedIndex = 0;
            }
        }

        #endregion

        public void SampleDocumentTreeViewLoad()
        {
            this.Invoke(new MethodInvoker(() => treeViewSampleDocument.Enabled = true));
            this.Invoke(new MethodInvoker(() => treeViewSampleDocument.Nodes.Clear()));

            supplierSampleDocument.Remove("_id");
            string json = supplierSampleDocument.ToJson();
            XmlDocument doc = JsonConvert.DeserializeXmlNode(json, "root");
            //XmlDocument doc = supplierCollection.GetSampleXml();

            this.Invoke(new MethodInvoker(() => ConvertXmlNodeToTreeNode(doc, treeViewSampleDocument.Nodes)));
            //ConvertXmlNodeToTreeNode(doc, treeViewSampleDocument.Nodes);

            this.Invoke(new MethodInvoker(() => treeViewSampleDocument.Nodes[0].Expand()));
            //treeViewSampleDocument.Nodes[0].Expand();
            this.Invoke(new MethodInvoker(() => treeViewSampleDocument.Nodes[0].Nodes[0].Expand()));
            //treeViewSampleDocument.Nodes[0].Nodes[0].Expand();
        }

        private void ConvertXmlNodeToTreeNode(XmlNode xmlNode, TreeNodeCollection treeNodes)
        {

            TreeNode newTreeNode = treeNodes.Add(xmlNode.Name);

            switch (xmlNode.NodeType)
            {
                case XmlNodeType.ProcessingInstruction:
                case XmlNodeType.XmlDeclaration:
                    newTreeNode.Text = "<?" + xmlNode.Name + " " +
                      xmlNode.Value + "?>";
                    break;
                case XmlNodeType.Element:
                    newTreeNode.Text = "" + xmlNode.Name + "";
                    break;
                case XmlNodeType.Attribute:
                    newTreeNode.Text = "ATTRIBUTE: " + xmlNode.Name;
                    break;
                case XmlNodeType.Text:
                case XmlNodeType.CDATA:
                    newTreeNode.Text = xmlNode.Value;
                    break;
                case XmlNodeType.Comment:
                    newTreeNode.Text = "<!--" + xmlNode.Value + "-->";
                    break;
            }

            if (xmlNode.Attributes != null)
            {
                foreach (XmlAttribute attribute in xmlNode.Attributes)
                {
                    ConvertXmlNodeToTreeNode(attribute, newTreeNode.Nodes);
                }
            }
            foreach (XmlNode childNode in xmlNode.ChildNodes)
            {
                ConvertXmlNodeToTreeNode(childNode, newTreeNode.Nodes);
            }
        }




        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (cmbSelectedId.SelectedIndex < 0 && cmbSelectedSearchField.SelectedIndex < 0)
            {
                MessageBox.Show("Please select search field and id field!");
            }
            else if (cmbSelectedSearchField.SelectedItem.ToString() == cmbSelectedId.SelectedItem.ToString())
            {
                MessageBox.Show("Search and Id field must be different!");
            }
            else
            {
                
                //items = supplierCollection.GetCollectionItems(cmbSelectedId.SelectedItem.ToString(), cmbSelectedSearchField.SelectedItem.ToString());

                if (includeSupplierAsSrchTxt.Checked)
                {
                    addOnSearchText = GetCmbSupplierDBSelectedName().ToLower().Replace("PD", "").Replace("_","");
                }

                var threadLoadSearchResultsDatagrid = new Thread(() => LoadSearchResultsDatagrid());
                threadLoadSearchResultsDatagrid.Start();

                    //LoadSearchResultsDatagrid();

                    //MessageBox.Show("Search Result loaded!");

                    //txtCategoryId.Enabled = true;
                    //btnUpdate.Enabled = true;
            }
        }

        private int currentSearchItemCounter = 0;
        Dictionary<string, SearchData> searchReadyItems = null;
        KeyValuePair<string, SearchData> currentSearchItem = new KeyValuePair<string, SearchData>();
        private void LoadSearchResultsDatagrid()
        {
            this.Invoke(new MethodInvoker(() => progressBar.Visible = true));
            this.Invoke(new MethodInvoker(() => progressBar.Maximum = 100));
            this.Invoke(new MethodInvoker(() => progressBar.Value = 10));

            this.Invoke(new MethodInvoker(() => dgvSearchResults.Rows.Clear()));
            //dgvSearchResults.Rows.Clear();
            this.Invoke(new MethodInvoker(() => dgvSearchResults.Refresh()));
            //dgvSearchResults.Refresh();

            if (searchReadyItems == null)
            {
                searchReadyItems = new Dictionary<string, SearchData>();
                ISearchReadyItems searchReadyItemsWorker = appConfiguration.Resolve<ISearchReadyItems>();

                string selectedId = "";
                this.Invoke(new MethodInvoker(() => selectedId = cmbSelectedId.SelectedItem.ToString()));
                string selectedSearchField = "";
                this.Invoke(new MethodInvoker(() => selectedSearchField = cmbSelectedSearchField.SelectedItem.ToString()));

                searchReadyItems = searchReadyItemsWorker.GetItems(selectedSupplierDB, selectedId, selectedSearchField);
                this.Invoke(new MethodInvoker(() => progressBar.Value = 40));
            }

            if (searchReadyItems.Count() > 0)
            {
                  
                    if (currentSearchItemCounter < searchReadyItems.Count())
                    {
                        currentSearchItem = searchReadyItems.ElementAt(currentSearchItemCounter);

                        //EbayWebsite ebaywebsite = new EbayWebsite();
                        //List<SearchResult> searchresults = ebaywebsite.Search(currentSearchItem.Value.searchString);

                        IEbaySearcher ebaySearcher = appConfiguration.Resolve<IEbaySearcher>();
                        searchresults = ebaySearcher.Search(addOnSearchText + " " + currentSearchItem.Value.searchString).ToList();
                        this.Invoke(new MethodInvoker(() => progressBar.Value = 80));

                        try
                        {
                            pictureBox1.Load("http://136.243.19.216:81/" + GetCmbSupplierDBSelectedName().ToLower().Replace("pd", "").Replace("_", "") + "/" + currentSearchItem.Key.ToString() + "_1.jpg");
                        }
                        catch {
                            pictureBox1.Load("https://d13yacurqjgara.cloudfront.net/users/24885/screenshots/1799551/page-cannot-be-found_1x.png");
                        }

                        foreach (SearchResult s in searchresults)
                        {
                            if (s.categories.Count() > 0)
                            {
                                IImageWorker imageWorker = appConfiguration.Resolve<IImageWorker>();
                                Image image = imageWorker.GetImage<string>(s.imagelink);

                                this.Invoke(new MethodInvoker(() => 
                                    dgvSearchResults.Rows.Add(image, s.title, s.price, s.categories.Last().categoryText, s.categories.Last().categoryNumber)
                                    ));
                                //dgvSearchResults.Rows.Add(image, s.title, s.price, s.categories.Last().categoryText, s.categories.Last().categoryNumber);
                            }
                        }

                        this.Invoke(new MethodInvoker(() => progressBar.Value = 90));

                        this.Invoke(new MethodInvoker(() => txtSearchID.Text = currentSearchItem.Key.ToString()));
                        //txtSearchID.Text = currentSearchItem.Key.ToString();
                        this.Invoke(new MethodInvoker(() => txtSearchString.Text = currentSearchItem.Value.searchString));
                        //txtSearchString.Text = currentSearchItem.Value.searchString;

                        currentSearchItemCounter++;
                    }
                    else
                    {
                        string selectedDBName = "";
                        this.Invoke(new MethodInvoker(() => selectedDBName = cmbSelectedDBName.SelectedItem.ToString()));
                        MessageBox.Show("Finished for " + selectedDBName);
                    }
                }
            else
            {
                MessageBox.Show("No item results from database, please select other supplier database!");
            }

            this.Invoke(new MethodInvoker(() => progressBar.Value = 100));

            Thread.Sleep(1500);

            this.Invoke(new MethodInvoker(() => btnSearchNext.Enabled = true));

            this.Invoke(new MethodInvoker(() => progressBar.Visible = false));

            this.Invoke(new MethodInvoker(() => progressBar.Value = 0));
        }

        List<SearchResult> searchresults = new List<SearchResult>();
        private void dgvSearchResults_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           
                string categoryid = dgvSearchResults.Rows[e.RowIndex].Cells[4].Value.ToString();
                DialogResult dialogResult = MessageBox.Show("You are about to update " + currentSearchItem.Key.ToString() + " to be in category " + categoryid, "Are you sure!", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    SearchResult clickedSearchResult = searchresults[e.RowIndex];

                    List<string> eans = new List<string>();
                    eans.Add(currentSearchItem.Key.ToString().Replace("\"",""));

                    CategoryPersistenceEntity categoryPersistenceEntity = new CategoryPersistenceEntity
                    {
                        specificcategory = clickedSearchResult.categories.Last().categoryNumber.ToString(),
                        eans = eans,
                        categories = clickedSearchResult.categories
                    };

                    //EbayCategoriesCollection.Insert(new EbayCategoryEntity
                    //{
                    //    eans = eans,
                    //    category = categoryid
                    //});

                    ICategoryPersist categoryPersist = appConfiguration.Resolve<ICategoryPersist>();
                    categoryPersist.Save(categoryPersistenceEntity);


                    //supplierCollection.UpdateEbayCategory(currentSearchItem.Key.ToString(), currentSearchItem.Value.idField.ToString());

                    ISupplierCategoryIndicator supplierCategoryIndicator = appConfiguration.Resolve<ISupplierCategoryIndicator>();
                    supplierCategoryIndicator.Update(currentSearchItem.Value.idField.ToString(), currentSearchItem.Key.ToString());

                    MessageBox.Show("Updated!");
                    NextItem();
                }
                else if (dialogResult == DialogResult.No)
                {
                    //do something else
                }
            
        }

        private void btnSearchNext_Click(object sender, EventArgs e)
        {
            NextItem();
        }

        private void NextItem()
        {
            var threadLoadSearchResultsDatagrid = new Thread(() => LoadSearchResultsDatagrid());
            threadLoadSearchResultsDatagrid.Start();
            //LoadSearchResultsDatagrid();
        }



        


    }
}
