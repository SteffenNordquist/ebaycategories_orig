﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesManual.ImageWorker
{
    public interface IImageWorker
    {
        Image GetImage<TParam>(TParam obj);
    }
}
