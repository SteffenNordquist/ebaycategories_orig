﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesManual.ImageWorker
{
    public class RemoteImageWorker : IImageWorker
    {
        public Image GetImage<TParam>(TParam obj)
        {
            string imageUrl = (string)(object)obj;

            System.Net.WebRequest request =
                System.Net.WebRequest.Create(imageUrl);

            System.Net.WebResponse response = request.GetResponse();
            System.IO.Stream responseStream =
                response.GetResponseStream();

            Bitmap bmp = new Bitmap(responseStream);

            responseStream.Dispose();

            return bmp;
        }
    }
}
