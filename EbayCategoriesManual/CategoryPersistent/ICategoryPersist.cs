﻿using EbayCategoriesManual.Ebay.Category;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesManual.CategoryPersistent
{
    public interface ICategoryPersist
    {
        void Save(CategoryPersistenceEntity category);
    }
}
