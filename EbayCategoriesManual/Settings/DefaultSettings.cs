﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesManual.Settings
{
    public class DefaultSettings : ISettings
    {
        private Dictionary<string, string> _settings;

        public DefaultSettings()
        {
            this._settings = new Dictionary<string, string>();
        }

        public void SetValue(string name, string value)
        {
            if (this._settings.ContainsKey(name))
            {
                this._settings[name] = value;
            }
            else
            {
                this._settings.Add(name, value);
            }
        }

        public string GetValue(string name)
        {
            if (!this._settings.ContainsKey(name))
            {
                return "";
            }

            return this._settings[name];
        }
    }
}
