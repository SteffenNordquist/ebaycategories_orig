﻿using DN.DataAccess.ConnectionFactory;
using EbayCategoriesManual.Document.ElementParser;
using EbayCategoriesManual.Entities;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesManual.SearchReadyItems
{
    public class DefaultSearchReadyItems : ISearchReadyItems
    {
        private readonly IElementParser documentElementParser;

        public DefaultSearchReadyItems(IElementParser documentElementParser)
        {
            this.documentElementParser = documentElementParser;
        }

        public Dictionary<string, SearchData> GetItems(IDbConnection selectedSupplierDB, string idField, string searchField)
        {
            string query = "{ \"plus.ebaycategory\" : {$exists:false}}";
            string[] setFields = { idField, searchField};

            return selectedSupplierDB.DataAccess.Queries.Find<BsonDocument>(query, setFields, 0, 10000)
                .ToDictionary(x => documentElementParser.GetElement(x, idField, true), x => new SearchData { searchString = documentElementParser.GetElement(x, searchField), idField = idField });
        }
    }
}
