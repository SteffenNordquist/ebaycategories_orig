﻿using DN.DataAccess.ConnectionFactory;
using EbayCategoriesManual.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesManual.SearchReadyItems
{
    public interface ISearchReadyItems
    {
        Dictionary<string, SearchData> GetItems(IDbConnection selectedSupplierDB, string idField, string searchField);
    }
}
