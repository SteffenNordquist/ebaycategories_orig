﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesManual.Logger
{
    public interface ILogger : IDisposable
    {
        void Log(string log);
    }
}
