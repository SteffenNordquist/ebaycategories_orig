﻿
using EbayCategoriesManual.CategoryPersistent;
using EbayCategoriesManual.SupplierDBConstructor;
using EbayCategoriesManual.Logger;
using EbayCategoriesManual.Suppliers;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCategoriesManual.Ebay.Html;
using EbayCategoriesManual.Ebay.Category;
using EbayCategoriesManual.Ebay.Category.Parser;
using EbayCategoriesManual.Settings;
using EbayCategoriesManual.Document;
using EbayCategoriesManual.ImageWorker;
using EbayCategoriesManual.Document.FieldsSplitter;
using EbayCategoriesManual.Document.ElementParser;
using EbayCategoriesManual.SearchReadyItems;
using EbayCategoriesManual.Ebay.Searcher;

namespace EbayCategoriesManual
{
    public class AppConfiguration
    {
        private readonly IUnityContainer container;

        public AppConfiguration(IUnityContainer container)
        {
            this.container = container;
            RegisterDependencies();
        }

        private void RegisterDependencies()
        {
            container.RegisterType<ILogger, DefaultLogger>();
            container.RegisterType<ISuppliers, BKSuppliers>();
            container.RegisterType<IDBConstructor, BKSupplierDBConstructor>();
            container.RegisterType<IDocumentFieldsSplitter, DefaultDocumentFieldsSplitter>();
            container.RegisterType<IElementParser, DefaultElementParser>();
            container.RegisterType<ISearchReadyItems, DefaultSearchReadyItems>();
            container.RegisterType<IImageWorker, RemoteImageWorker>();
            container.RegisterType<ISupplierIdList, BKSupplierIdList>();

            container.RegisterType<IHtmlDownloader, HtmlDownloader>();
            container.RegisterType<ICategoryPersist, CategoryPersist>();
            container.RegisterType<ICategoryNumberParser, CategoryNumberParser>();
            container.RegisterType<ICategoryTextParser, CategoryTextParser>();
            container.RegisterType<ICategoryLooker, CategoryLooker>();
            container.RegisterType<ISupplierCategoryIndicator, SupplierCategoryIndicator>();

            container.RegisterType<IEbaySearcher, DefaultEbaySearcher>();

            //container.RegisterType<IEbayWorker, EbayWorker>();
        }

        public void RegisterSettings(ISettings obj)
        {
            container.RegisterInstance(obj);
        }
        
        public T Resolve<T>()
        {
            return container.Resolve<T>();
        }


    }
}
