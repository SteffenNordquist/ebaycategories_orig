﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesManual.Document.ElementParser
{
    public interface IElementParser
    {
        string GetElement(BsonDocument document, string field, bool isIdField = false, int idLength = 13);
    }
}
