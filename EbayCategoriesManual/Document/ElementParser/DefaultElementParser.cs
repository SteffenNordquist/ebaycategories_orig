﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EbayCategoriesManual.Document.ElementParser
{
    public class DefaultElementParser : IElementParser
    {
        public string GetElement(BsonDocument document, string field, bool isIdField = false, int idLength = 13)
        {
            BsonValue documentBsonValue = document;
            BsonValue val = null;
            string[] split = field.Split('.');

            int counter = 1;
            foreach (string f in split)
            {
                if (counter == split.Count())
                {
                    val = document.GetElement(f).Value;
                }
                else
                {
                    if (document.IsBsonArray)
                    {
                        if (isIdField)
                        {
                            foreach (BsonDocument subDocument in document.AsBsonArray)
                            {
                                Regex eanRegex = new Regex(@"\d{13}");
                                if (eanRegex.IsMatch(subDocument.ToJson().ToString()))
                                {
                                    document = subDocument.GetElement(f).Value.ToBsonDocument();
                                    break;
                                }
                                
                            }
                        }
                        else
                        {
                            document = document.AsBsonArray[0].AsBsonDocument.GetElement(f).Value.ToBsonDocument();
                        }
                    }
                    else
                    {
                        if (document.Contains(f))
                        {
                                document = (BsonDocument)document.GetElement(f).Value;
                           
                        }
                    }
                }
                counter++;
            }

            return val.ToString();
        }
    }
}
