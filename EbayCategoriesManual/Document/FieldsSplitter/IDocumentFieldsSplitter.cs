﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesManual.Document.FieldsSplitter
{
    public interface IDocumentFieldsSplitter
    {
        List<string> GetFields<T>(T document);
    }
}
