﻿using EbayCategoriesManual.Entities;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesManual
{
    public class EbayWebsite
    {
        private List<string> eans = new List<string>();
        private WebClient webclient = null;
        private List<EbayCategoryEntity> ebaycategories = new List<EbayCategoryEntity>();

        public EbayWebsite(List<string> eans)
        {
            this.eans = eans;
            this.webclient = new WebClient();
            this.webclient.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
           
        }

        public void ProcessEans()
        {
            
            foreach (string ean in eans)
            {
                List<string> eanss = new List<string>();
                eanss.Add(ean);

                ebaycategories.Add(new EbayCategoryEntity { eans = eanss, category = GetCategory(ean) });
            }

        }

        public EbayWebsite()
        {
        }

        public List<EbayCategoryEntity> GetEbayCategories()
        {
            return ebaycategories.Select(x => x).Where(x => x.category != "").ToList();
        }

        private string GetCategory(string ean)
        {
            string link = String.Format("http://www.ebay.de/sch/i.html?_from=R40&_sacat=0&_nkw={0}&_rdc=1", ean);

            string pagesource = GetPageSource(link);

            //string pagesource = webclient.DownloadString(ebaysearchLink);

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(pagesource);

            var listingNodes = doc.DocumentNode.SelectNodes("//li[@class='sresult lvresult clearfix li']");

            if (listingNodes != null && listingNodes.Count() > 0)
            {
                var listingNode = listingNodes.First();
                string a = listingNode.SelectSingleNode(listingNode.XPath + "//a").Attributes["href"].Value.ToString();

                //pagesource = webclient.DownloadString(a);
                pagesource = GetPageSource(a);
                doc.LoadHtml(pagesource);

                var lastCatNode = doc.DocumentNode.SelectNodes("//li[@class='bc-w']").Last();
                string itemlink = lastCatNode.SelectSingleNode(lastCatNode.XPath + "/a").Attributes["href"].Value.ToString();

                string[] split = itemlink.Split('/');
                string ebaycategory = split[split.Count() - 2];

                return ebaycategory;
            }

            return string.Empty;
        }

        public List<SearchResult> Search(string str)
        {
            List<SearchResult> searchresults = new List<SearchResult>();
            string link = String.Format("http://www.ebay.de/sch/i.html?_from=R40&_sacat=0&_nkw={0}&_rdc=1", str);
            string pagesource = GetPageSource(link);


            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(pagesource);

            var listingNodes = doc.DocumentNode.SelectNodes("//li[@class='sresult lvresult clearfix li']");

            if (listingNodes != null && listingNodes.Count() > 0)
            {
                foreach (var listingNode in listingNodes.Take(10))
                {
                    string itemlink = listingNode.SelectSingleNode(listingNode.XPath + "//a").Attributes["href"].Value.ToString();
                    string imagelink = listingNode.SelectSingleNode(listingNode.XPath + "//img").Attributes["src"].Value.ToString();
                    string title = listingNode.SelectSingleNode(listingNode.XPath + "//h3[@class='lvtitle']").InnerText.Trim();
                    string price = listingNode.SelectSingleNode(listingNode.XPath + "//li[@class='lvprice prc']").InnerText.Trim();

                    searchresults.Add(new SearchResult
                    {
                        itemlink = itemlink,
                        imagelink = imagelink,
                        title = title,
                        price = price
                    });

                    
                }
            }

            GetSearchResultsCategories(ref searchresults);

            return searchresults;
        }
        private void GetSearchResultsCategories(ref List<SearchResult> searchresults)
        {
            HtmlDocument doc = new HtmlDocument();

            foreach (SearchResult s in searchresults)
            {
                string pagesource = GetPageSource(s.itemlink);
                doc = new HtmlDocument();
                doc.LoadHtml(pagesource);

                //var categoryNode = doc.DocumentNode.SelectSingleNode("//td[@class='vi-VR-brumb-lnkLst']");
                //var categoryNodes = doc.DocumentNode.SelectNodes("//li[@class='bc-w']");
                var lastCatNode = doc.DocumentNode.SelectNodes("//li[@class='bc-w']").Last();
                HtmlNode ebaycatnode = lastCatNode.SelectSingleNode(lastCatNode.XPath + "/a");
                string ebaycatlink = ebaycatnode.Attributes["href"].Value.ToString();
                string ebaycategoryname = ebaycatnode.InnerText.Trim();

                string[] split = ebaycatlink.Split('/');
                string ebaycategory = split[split.Count() - 2];

                s.categoryid = ebaycategory;
                s.categoryname = ebaycategoryname;
            }
        }

        private string GetPageSource(string link)
        {
            //string link = String.Format("http://www.ebay.de/sch/i.html?_from=R40&_sacat=0&_nkw={0}&_rdc=1", ean);
            //WebRequest request = HttpWebRequest.Create(link);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(link);
            request.Timeout = 20 * 60 * 1000;
            request.KeepAlive = false;
            request.ProtocolVersion = HttpVersion.Version10; 
            //var response = r.GetResponse();
            
            string pagesource = "";
            //StreamReader readStream = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
            using (WebResponse response = request.GetResponse())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    using (StreamReader readStream = new StreamReader(stream, Encoding.UTF8))
                    {
                        pagesource = readStream.ReadToEnd();
                    }
                }
            }

            //response.Dispose();
            //readStream.Dispose();

            return pagesource;
        }
    }
}
