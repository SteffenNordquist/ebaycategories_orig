﻿using EbayCategoriesManual.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesManual.Ebay.Searcher
{
    public interface IEbaySearcher
    {
        IEnumerable<SearchResult> Search(string text);
    }
}
