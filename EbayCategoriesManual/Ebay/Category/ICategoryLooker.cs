﻿
using EbayCategoriesManual.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesManual.Ebay.Category
{
    public interface ICategoryLooker
    {
        IEnumerable<CategoryEntity> GetCategories(string itemlink);
    }
}
