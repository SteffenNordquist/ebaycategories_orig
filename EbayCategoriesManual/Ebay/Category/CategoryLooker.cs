﻿using EbayCategoriesManual.Ebay.Html;
using EbayCategoriesManual.Ebay.Category.Parser;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCategoriesManual.Entities;

namespace EbayCategoriesManual.Ebay.Category
{
    public class CategoryLooker : ICategoryLooker
    {
        private readonly IHtmlDownloader htmlDownloader;
        private readonly ICategoryTextParser categoryTextParser;
        private readonly ICategoryNumberParser categoryNumberParser;

        public CategoryLooker(IHtmlDownloader htmlDownloader, ICategoryTextParser categoryTextParser, ICategoryNumberParser categoryNumberParser)
        {
            this.htmlDownloader = htmlDownloader;
            this.categoryTextParser = categoryTextParser;
            this.categoryNumberParser = categoryNumberParser;
        }

        public IEnumerable<CategoryEntity> GetCategories(string itemlink)
        {
            List<CategoryEntity> categories = new List<CategoryEntity>();

            HtmlDocument doc = new HtmlDocument();

             string pageSource = htmlDownloader.GetHtml(itemlink);

                    if (pageSource != "")
                    {
                        doc.LoadHtml(pageSource);
                        
                        var categoryNodes = doc.DocumentNode.SelectNodes("//li[@class='bc-w']");

                        if (categoryNodes != null)
                        {
                            int counter = 0;
                            foreach (var categoryNode in categoryNodes)
                            {
                                var categoryLinkNode = categoryNode.SelectSingleNode(categoryNode.XPath + "/a");

                                if (categoryLinkNode != null)
                                {
                                    categories.Add(new CategoryEntity
                                    {
                                        index = counter,
                                        categoryNumber = categoryNumberParser.GetCategoryNumber(categoryLinkNode),
                                        categoryText = categoryTextParser.GetCategoryText(categoryLinkNode)
                                    });

                                    counter++;
                                }
                            }
                        }

                    }

            return categories;
        }
    }
}
