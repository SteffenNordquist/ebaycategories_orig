﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesManual.Ebay.Category.Parser
{
    public interface ICategoryNumberParser
    {
        int GetCategoryNumber(HtmlNode sourceNode);
    }
}
