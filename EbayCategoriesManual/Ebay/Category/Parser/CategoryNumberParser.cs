﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesManual.Ebay.Category.Parser
{
    public class CategoryNumberParser : ICategoryNumberParser
    {
        public int GetCategoryNumber(HtmlNode sourceNode)
        {
            string itemlink = sourceNode.Attributes["href"].Value.ToString();

            string[] split = itemlink.Split('/');

            string ebaycategory = split[split.Count() - 2];

            return int.Parse(ebaycategory.Trim());
        }
    }
}
