﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesManual.Entities
{
    public class SearchResult
    {
        public string imagelink { get; set; }
        public string itemlink { get; set; }
        public string title { get; set; }
        public string price { get; set; }
        public List<CategoryEntity> categories { get; set; }
    }
}
