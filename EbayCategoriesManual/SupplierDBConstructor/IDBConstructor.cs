﻿using DN.DataAccess.ConnectionFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesManual.SupplierDBConstructor
{
    public interface IDBConstructor
    {
        IDbConnection Create<TSource>(TSource value);
    }
}
