﻿using DN.DataAccess;
using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.ConnectionFactory.Mongo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesManual.SupplierDBConstructor
{
    public class BKSupplierDBConstructor : IDBConstructor
    {
        public IDbConnection Create<TSource>(TSource value)
        {
            string dbName = (string)(object)value;

            #region dn data access
            IDbConnectionFactory connectionFactory = new MongoConnectionFactory(new MongoConnectionProperties());
            connectionFactory.ConnectionProperties.SetProperty("connectionstring", String.Format("mongodb://client148:client148devnetworks@136.243.44.111/{0}", dbName));
            connectionFactory.ConnectionProperties.SetProperty("databasename", dbName);
            string colname = dbName.Replace("PD_", "").ToLower().Replace("berk", "berks").Replace("bremer", "bremeritems").Replace("knv", "knvbooks").Replace("flotten24", "flotten");
            connectionFactory.ConnectionProperties.SetProperty("collectionname", colname);
            #endregion

            return connectionFactory.CreateConnection();
        }
    }
}
