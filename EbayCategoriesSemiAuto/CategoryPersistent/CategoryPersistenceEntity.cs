﻿using EbayCategoriesSemiAuto.Category;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesSemiAuto.CategoryPersistent
{
    [BsonIgnoreExtraElements]
    public class CategoryPersistenceEntity
    {
        [BsonId]
        public string specificcategory { get; set; }
        public List<string> eans { get; set; }
        public List<CategoryEntity> categories { get; set; }
    }
}
