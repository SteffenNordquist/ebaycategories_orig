﻿using DN.DataAccess;
using DN.DataAccess.ConfigurationData;
using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.ConnectionFactory.Mongo;
using EbayCategoriesSemiAuto.SupplierDBConstructor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesSemiAuto.CategoryPersistent
{
    public class CategoryPersist : ICategoryPersist
    {
        private readonly IDbConnection dbConnection;

        public CategoryPersist()
        {
            #region dn data access
            IConfigurationManager configurationManager = new NullConfigurationManager();

            IConnectionProperties connectionProperties = new MongoConnectionProperties();
            connectionProperties.SetProperty("connectionstring", String.Format("mongodb://client148:client148devnetworks@136.243.44.111/{0}", "EbayCategories"));
            connectionProperties.SetProperty("databasename", "EbayCategories");
            connectionProperties.SetProperty("collectionname", "ebaycategoriespaths");

            IDbConnectionFactory connectionFactory = new MongoConnectionFactory(connectionProperties);
            #endregion

            dbConnection = connectionFactory.CreateConnection();
        }

        public void Save(CategoryPersistenceEntity category)
        {

            var testExistence = dbConnection.DataAccess.Queries.FindByKey<CategoryPersistenceEntity>("_id", category.specificcategory);
            if (testExistence == null)
            {
                dbConnection.DataAccess.Commands.Insert<CategoryPersistenceEntity>(category);
            }
            else
            {
                var eanExistence = dbConnection.DataAccess.Queries.FindByKey<CategoryPersistenceEntity>("eans", category.eans.First());
                if (eanExistence == null)
                {
                    string updatequery = "{ _id : \"" + category.specificcategory + "\" }";
                    dbConnection.DataAccess.Commands.AddToSet<string>(updatequery, "eans", category.eans.First());
                }
            }
        }
    }
}
