﻿using EbayCategoriesSemiAuto.Category;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesSemiAuto.CategoryPersistent
{
    public interface ICategoryPersist
    {
        void Save(CategoryPersistenceEntity category);
    }
}
