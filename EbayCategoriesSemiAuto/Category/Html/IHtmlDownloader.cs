﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesSemiAuto.Category.Html
{
    public interface IHtmlDownloader
    {
        string GetHtml(string link);
    }
}
