﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesSemiAuto.Category.Parser
{
    public class CategoryTextParser : ICategoryTextParser
    {
        public string GetCategoryText(HtmlNode sourceNode)
        {
            return sourceNode.InnerText.Trim();
        }
    }
}
