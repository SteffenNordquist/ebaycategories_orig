﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesSemiAuto.Category.Parser
{
    public interface ICategoryTextParser
    {
        string GetCategoryText(HtmlNode sourceNode);
    }
}
