﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesSemiAuto.Category
{
    public class CategoryEntity
    {
        public int index { get; set; }
        public int categoryNumber { get; set; }
        public string categoryText { get; set; }
    }
}
