﻿using EbayCategoriesSemiAuto.Category.Html;
using EbayCategoriesSemiAuto.Category.Parser;
using EbayCategoriesSemiAuto.Ebay;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesSemiAuto.Category
{
    public class CategoryLooker : ICategoryLooker
    {
        private readonly IHtmlDownloader htmlDownloader;
        private readonly ICategoryTextParser categoryTextParser;
        private readonly ICategoryNumberParser categoryNumberParser;

        public CategoryLooker(IHtmlDownloader htmlDownloader, ICategoryTextParser categoryTextParser, ICategoryNumberParser categoryNumberParser)
        {
            this.htmlDownloader = htmlDownloader;
            this.categoryTextParser = categoryTextParser;
            this.categoryNumberParser = categoryNumberParser;
        }

        public IEnumerable<CategoryEntity> GetCategories(string id)
        {
            List<CategoryEntity> categories = new List<CategoryEntity>();

            string ebaysearchLink = String.Format("http://www.ebay.de/sch/i.html?_from=R40&_sacat=0&_nkw={0}&_rdc=1", id);

            string pageSource = htmlDownloader.GetHtml(ebaysearchLink);

            if (pageSource != "")
            {
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(pageSource);

                List<HtmlNode> listingNodes = new List<HtmlNode>();

                var resultsContainer = doc.DocumentNode.SelectSingleNode("//ul[@id='ListViewInner']");

                if (resultsContainer != null)
                {
                    var li_elements = resultsContainer.Elements("li");

                    if (li_elements != null && li_elements.Count() > 0)
                    {
                        listingNodes = li_elements.Where(x => x.Attributes["class"].Value.Contains("sresult lvresult clearfix li")).Select(x => x).ToList();

                        if (li_elements.Count() > 1)
                        {
                        }
                    }

                }

                //HtmlNodeCollection
                //listingNodes = doc.DocumentNode.SelectNodes("//li[@class='sresult lvresult clearfix li']");

                if (listingNodes != null && listingNodes.Count() > 0)
                {
                    var listingNode = listingNodes.First();

                    string initialListingResultLink = listingNode.SelectSingleNode(listingNode.XPath + "//a").Attributes["href"].Value.ToString();

                    pageSource = htmlDownloader.GetHtml(initialListingResultLink);

                    if (pageSource != "")
                    {
                        doc.LoadHtml(pageSource);

                        //var lastCatNode = doc.DocumentNode.SelectNodes("//li[@class='bc-w']").Last();
                        //string itemlink = lastCatNode.SelectSingleNode(lastCatNode.XPath + "/a").Attributes["href"].Value.ToString();

                        //var categoryNodes = doc.DocumentNode.SelectNodes("//*[@id='vi-VR-brumb-lnkLst']/table/tbody/tr[1]/td/h2/ul/li");
                        var categoryNodes = doc.DocumentNode.SelectNodes("//li[@class='bc-w']");

                        if (categoryNodes != null)
                        {
                            int counter = 0;
                            foreach (var categoryNode in categoryNodes)
                            {
                                var categoryLinkNode = categoryNode.SelectSingleNode(categoryNode.XPath + "/a");

                                if (categoryLinkNode != null)
                                {
                                    categories.Add(new CategoryEntity
                                    {
                                        index = counter,
                                        categoryNumber = categoryNumberParser.GetCategoryNumber(categoryLinkNode),
                                        categoryText = categoryTextParser.GetCategoryText(categoryLinkNode)
                                    });

                                    counter++;
                                }
                            }
                        }

                    }

                }
            }

            return categories;
        }
    }
}
