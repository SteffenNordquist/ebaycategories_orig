﻿using DN.DataAccess;
using DN.DataAccess.ConnectionFactory;
using EbayCategoriesSemiAuto.SupplierDBConstructor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesSemiAuto.Suppliers
{
    public class SupplierCategoryIndicator : ISupplierCategoryIndicator
    {
        private IDbConnection dbConnection;
        private readonly IDBConstructor dbConstructor;
        private readonly ISettings settings;

        public SupplierCategoryIndicator(IDBConstructor dbConstructor, ISettings settings)
        {
            this.dbConstructor = dbConstructor;
            this.settings = settings;
            this.dbConnection = dbConstructor.Create<string>(settings.GetValue("dbname"));
        }

        public void Update(string idPath, string id)
        {
            string query = "{ \"" + idPath + "\" : \"" + id + "\"}";
            dbConnection.DataAccess.Commands.Update<string>(query, "plus.ebaycategory", idPath);
        }

    }
}
