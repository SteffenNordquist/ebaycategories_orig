﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesSemiAuto.Suppliers
{
    public class BKSuppliers : ISuppliers
    {
        public IEnumerable<string> GetSuppliers()
        {
            var server = new MongoClient("mongodb://steffennordquist:adminsteffen148devnetworks@136.243.44.111/admin")
                .GetServer();

            return server.GetDatabaseNames().Select(x => x.ToString()).Where(x => x.StartsWith("PD")).ToList();
        }
    }
}
