﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesSemiAuto.Suppliers
{
    public interface ISuppliers
    {
        IEnumerable<string> GetSuppliers();
    }
}
