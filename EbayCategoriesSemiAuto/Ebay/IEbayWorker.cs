﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesSemiAuto.Ebay
{
    public interface IEbayWorker
    {
        void ProcessIds(IEnumerable<string> ids, string idFieldPath, bool encloseIdWithQuotes);
    }
}
