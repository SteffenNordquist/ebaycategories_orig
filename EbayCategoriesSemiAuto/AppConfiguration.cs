﻿using EbayCategoriesSemiAuto.Category;
using EbayCategoriesSemiAuto.Category.Html;
using EbayCategoriesSemiAuto.Category.Parser;
using EbayCategoriesSemiAuto.CategoryPersistent;
using EbayCategoriesSemiAuto.SupplierDBConstructor;
using EbayCategoriesSemiAuto.Ebay;
using EbayCategoriesSemiAuto.Logger;
using EbayCategoriesSemiAuto.Suppliers;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesSemiAuto
{
    public class AppConfiguration
    {
        private readonly IUnityContainer container;

        public AppConfiguration(IUnityContainer container)
        {
            this.container = container;
            RegisterDependencies();
        }

        private void RegisterDependencies()
        {
            container.RegisterType<ILogger, DefaultLogger>();
            container.RegisterType<ISuppliers, BKSuppliers>();
            container.RegisterType<IDBConstructor, BKSupplierDBConstructor>();
            container.RegisterType<ISupplierIdList, BKSupplierIdList>();
            
            container.RegisterType<IHtmlDownloader, HtmlDownloader>();
            container.RegisterType<ICategoryPersist, CategoryPersist>();
            container.RegisterType<ICategoryNumberParser, CategoryNumberParser>();
            container.RegisterType<ICategoryTextParser, CategoryTextParser>();
            container.RegisterType<ICategoryLooker, CategoryLooker>();
            container.RegisterType<ISupplierCategoryIndicator, SupplierCategoryIndicator>();

            container.RegisterType<IEbayWorker, EbayWorker>();
        }

        public void RegisterSettings(ISettings obj)
        {
            container.RegisterInstance(obj);
        }
        
        public T Resolve<T>()
        {
            return container.Resolve<T>();
        }


    }
}
