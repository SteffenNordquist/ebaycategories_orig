﻿using DN.DataAccess;
using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.MongoDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCategoriesSemiAuto.DBConstructor
{
    public class BKDBConstructor : IDBConstructor
    {
        public IDbConnection Create<TSource>(TSource value)
        {
            string dbName = (string)(object)value;

            #region dn data access
            IConfigurationManager configurationManager = new NullConfigurationManager();

            IConnectionProperties connectionProperties = new MongoConnectionProperties(configurationManager);
            connectionProperties.SetProperty("connectionstring", String.Format("mongodb://client148:client148devnetworks@136.243.44.111/{0}", dbName));
            connectionProperties.SetProperty("databasename", dbName);
            
            string colname = dbName.Replace("PD_", "").ToLower().Replace("berk", "berks").Replace("bremer", "bremeritems").Replace("knv", "knvbooks").Replace("flotten24", "flotten");
            connectionProperties.SetProperty("collectionname", colname);

            IDbConnectionFactory connectionFactory = new MongoConnectionFactory(connectionProperties);
            #endregion

            return connectionFactory.CreateConnection();
        }
    }
}
