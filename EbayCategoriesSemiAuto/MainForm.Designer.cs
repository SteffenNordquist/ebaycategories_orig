﻿namespace EbayCategoriesSemiAuto
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGetCategoriesByEan = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBoxEanField = new System.Windows.Forms.TextBox();
            this.cmbBoxPDs = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.encloseEanWithQoutes = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtIdLength = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnGetCategoriesByEan
            // 
            this.btnGetCategoriesByEan.Location = new System.Drawing.Point(36, 220);
            this.btnGetCategoriesByEan.Name = "btnGetCategoriesByEan";
            this.btnGetCategoriesByEan.Size = new System.Drawing.Size(360, 45);
            this.btnGetCategoriesByEan.TabIndex = 0;
            this.btnGetCategoriesByEan.Text = "Get Categories";
            this.btnGetCategoriesByEan.UseVisualStyleBackColor = true;
            this.btnGetCategoriesByEan.Click += new System.EventHandler(this.btnGetCategoriesByEan_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "DB Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Ean Path";
            // 
            // txtBoxEanField
            // 
            this.txtBoxEanField.Location = new System.Drawing.Point(103, 85);
            this.txtBoxEanField.Name = "txtBoxEanField";
            this.txtBoxEanField.Size = new System.Drawing.Size(293, 20);
            this.txtBoxEanField.TabIndex = 5;
            // 
            // cmbBoxPDs
            // 
            this.cmbBoxPDs.FormattingEnabled = true;
            this.cmbBoxPDs.Location = new System.Drawing.Point(103, 34);
            this.cmbBoxPDs.Name = "cmbBoxPDs";
            this.cmbBoxPDs.Size = new System.Drawing.Size(293, 21);
            this.cmbBoxPDs.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(100, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "eg: product.ean";
            // 
            // encloseEanWithQoutes
            // 
            this.encloseEanWithQoutes.AutoSize = true;
            this.encloseEanWithQoutes.Location = new System.Drawing.Point(36, 140);
            this.encloseEanWithQoutes.Name = "encloseEanWithQoutes";
            this.encloseEanWithQoutes.Size = new System.Drawing.Size(145, 17);
            this.encloseEanWithQoutes.TabIndex = 9;
            this.encloseEanWithQoutes.Text = "Enclose Ean with Quotes";
            this.encloseEanWithQoutes.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(33, 182);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Id length";
            // 
            // txtIdLength
            // 
            this.txtIdLength.Location = new System.Drawing.Point(103, 175);
            this.txtIdLength.Name = "txtIdLength";
            this.txtIdLength.Size = new System.Drawing.Size(293, 20);
            this.txtIdLength.TabIndex = 10;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 296);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtIdLength);
            this.Controls.Add(this.encloseEanWithQoutes);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbBoxPDs);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBoxEanField);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnGetCategoriesByEan);
            this.Name = "MainForm";
            this.Text = "Ebay Categories";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGetCategoriesByEan;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBoxEanField;
        private System.Windows.Forms.ComboBox cmbBoxPDs;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox encloseEanWithQoutes;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtIdLength;
    }
}

