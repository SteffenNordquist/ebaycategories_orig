﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using Microsoft.Practices.Unity;
using EbayCategoriesSemiAuto.Suppliers;
using EbayCategoriesSemiAuto.Ebay;
namespace EbayCategoriesSemiAuto
{
    public partial class MainForm : Form
    {

        private readonly AppConfiguration appConfiguration;

        public MainForm()
        {
            appConfiguration = new AppConfiguration(new UnityContainer());

            InitializeComponent();
        }

        private void btnGetCategoriesByEan_Click(object sender, EventArgs e)
        {
            if (cmbBoxPDs.SelectedIndex < 0 || txtBoxEanField.Text == string.Empty)
            {
                MessageBox.Show("Enter fields required!");
            }
            else
            {
                ISupplierIdList supplierIdList = appConfiguration.Resolve<ISupplierIdList>();

                bool hasMore = true;
                int limit = 1000;
                int skip = 0;
                while (hasMore)
                {
                    hasMore = false;

                    List<string> eans =
                        (List<string>) supplierIdList.GetIdList(txtBoxEanField.Text, cmbBoxPDs.SelectedItem.ToString(), skip, limit);

                    ISettings settings = new DefaultSettings();
                    settings.SetValue("dbname", cmbBoxPDs.SelectedItem.ToString());
                    appConfiguration.RegisterSettings(settings);

                    IEbayWorker ebayWorker = appConfiguration.Resolve<IEbayWorker>();

                    //process ids without quotation
                    //ebayWorker.ProcessIds(eans, txtBoxEanField.Text);

                    //process ids with quotation
                    //eans = (List<string>)supplierIdList.GetIdList(txtBoxEanField.Text, cmbBoxPDs.SelectedItem.ToString());

                    int idLength = 0;
                    Int32.TryParse(txtIdLength.Text, out idLength);

                    if (idLength > 0)
                    {
                        ebayWorker.ProcessIds(eans.Where(x => x.Length >= idLength), txtBoxEanField.Text,
                            encloseEanWithQoutes.Checked);
                    }
                    else
                    {
                        ebayWorker.ProcessIds(eans, txtBoxEanField.Text, encloseEanWithQoutes.Checked);
                    }

                    if (eans.Any())
                    {
                        hasMore = true;
                    }
                }

                MessageBox.Show("Done!");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            PopulateSupplierList();
        }

        private void PopulateSupplierList()
        {
            ISuppliers bkSuppliers = appConfiguration.Resolve<ISuppliers>();

            List<string> dbNames = (List<string>)bkSuppliers.GetSuppliers();

            dbNames.ForEach(x => cmbBoxPDs.Items.Add(x));
        }
    }
}
